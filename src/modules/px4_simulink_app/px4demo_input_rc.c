/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: px4demo_input_rc.c
 *
 * Code generated for Simulink model 'px4demo_input_rc'.
 *
 * Model version                  : 1.48
 * Simulink Coder version         : 9.0 (R2018b) 24-May-2018
 * C/C++ source code generated on : Wed Oct 31 17:41:37 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "px4demo_input_rc.h"
#include "px4demo_input_rc_private.h"

/* Block signals (default storage) */
B_px4demo_input_rc_T px4demo_input_rc_B;

/* Block states (default storage) */
DW_px4demo_input_rc_T px4demo_input_rc_DW;

/* Real-time model */
RT_MODEL_px4demo_input_rc_T px4demo_input_rc_M_;
RT_MODEL_px4demo_input_rc_T *const px4demo_input_rc_M = &px4demo_input_rc_M_;

/* Model step function */
void px4demo_input_rc_step(void)
{
  /* S-Function (sfun_px4_input_rc): '<Root>/input_rc' */
  {
    bool updated;
    orb_check(px4demo_input_rc_DW.input_rc_input_rc_fd.fd, &updated);
    if (updated) {
      struct rc_input_values pwm_inputs;

      /* copy input_rc raw data into local buffer (uint16)*/
      orb_copy(ORB_ID(input_rc), px4demo_input_rc_DW.input_rc_input_rc_fd.fd,
               &pwm_inputs);
      px4demo_input_rc_B.input_rc_o1 = pwm_inputs.values[2];
      px4demo_input_rc_B.input_rc_o2 = pwm_inputs.values[3];
    }
  }

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant1'
   */
  if (px4demo_input_rc_B.input_rc_o1 > 1500) {
    px4demo_input_rc_B.Switch = SL_MODE_BLINK_FAST;
  } else {
    px4demo_input_rc_B.Switch = SL_MODE_BLINK_NORMAL;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Switch: '<Root>/Switch1' incorporates:
   *  Constant: '<Root>/Constant2'
   *  Constant: '<Root>/Constant3'
   */
  if (px4demo_input_rc_B.input_rc_o2 > 1500) {
    px4demo_input_rc_B.Switch1 = SL_COLOR_BLUE;
  } else {
    px4demo_input_rc_B.Switch1 = SL_COLOR_RED;
  }

  /* End of Switch: '<Root>/Switch1' */

  /* S-Function (sfun_px4_rgbled): '<Root>/RGB_LED' */
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.mode = px4demo_input_rc_B.Switch;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.color =
    px4demo_input_rc_B.Switch1;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.num_blinks = 0;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.priority = 0;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.timestamp = hrt_absolute_time();
  orb_publish(ORB_ID(led_control), px4demo_input_rc_DW.RGB_LED_orb_advert_t ,
              &px4demo_input_rc_DW.RGB_LED_sl_led_control_s);
}

/* Model initialize function */
void px4demo_input_rc_initialize(void)
{
  /* Registration code */

  /* initialize real-time model */
  (void) memset((void *)px4demo_input_rc_M, 0,
                sizeof(RT_MODEL_px4demo_input_rc_T));

  /* block I/O */
  (void) memset(((void *) &px4demo_input_rc_B), 0,
                sizeof(B_px4demo_input_rc_T));

  {
    px4demo_input_rc_B.Switch = SL_MODE_OFF;
    px4demo_input_rc_B.Switch1 = SL_COLOR_OFF;
  }

  /* states (dwork) */
  (void) memset((void *)&px4demo_input_rc_DW, 0,
                sizeof(DW_px4demo_input_rc_T));

  /* Start for S-Function (sfun_px4_input_rc): '<Root>/input_rc' */
  {
    /* S-Function Block: <Root>/input_rc */
    /* subscribe to PWM RC input topic */
    int fd = orb_subscribe(ORB_ID(input_rc));
    px4demo_input_rc_DW.input_rc_input_rc_fd.fd = fd;
    px4demo_input_rc_DW.input_rc_input_rc_fd.events = POLLIN;
    orb_set_interval(fd, 1);
    PX4_INFO("* Subscribed to input_rc topic (fd = %d)*\n", fd);
  }

  /* Start for S-Function (sfun_px4_rgbled): '<Root>/RGB_LED' */
  {
    // enable RGBLED, set intitial mode and color
    // more devices will be 1, 2, etc
    px4demo_input_rc_DW.RGB_LED_sl_led_control_s.led_mask = 0xff;
    px4demo_input_rc_DW.RGB_LED_sl_led_control_s.mode = MODE_OFF;
    px4demo_input_rc_DW.RGB_LED_sl_led_control_s.priority = 0;
    px4demo_input_rc_DW.RGB_LED_sl_led_control_s.timestamp = hrt_absolute_time();
    px4demo_input_rc_DW.RGB_LED_orb_advert_t = orb_advertise_queue(ORB_ID
      (led_control), &px4demo_input_rc_DW.RGB_LED_sl_led_control_s,
      LED_UORB_QUEUE_LENGTH);
  }
}

/* Model terminate function */
void px4demo_input_rc_terminate(void)
{
  /* Terminate for S-Function (sfun_px4_input_rc): '<Root>/input_rc' */

  /* Close uORB service used in the S-Function Block: <Root>/input_rc */
  close(px4demo_input_rc_DW.input_rc_input_rc_fd.fd);

  /* Terminate for S-Function (sfun_px4_rgbled): '<Root>/RGB_LED' */

  /* Turn off LED */
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.led_mask = 0xff;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.mode = MODE_OFF;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.priority = 0;
  px4demo_input_rc_DW.RGB_LED_sl_led_control_s.timestamp = hrt_absolute_time();
  px4demo_input_rc_DW.RGB_LED_orb_advert_t = orb_advertise_queue(ORB_ID
    (led_control), &px4demo_input_rc_DW.RGB_LED_sl_led_control_s,
    LED_UORB_QUEUE_LENGTH);

  /* Close uORB service used in the S-Function Block: <Root>/RGB_LED */
  orb_unadvertise(px4demo_input_rc_DW.RGB_LED_orb_advert_t);
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
